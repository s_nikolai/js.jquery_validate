js.jquery_validate
==================

.. contents::

Introduction
------------

This library packages `jQuery Validation Plugin`_ for `fanstatic`_.

.. _`fanstatic`: http://fanstatic.org
.. _`jQuery Validation Plugin`: http://bassistance.de/jquery-plugins/jquery-plugin-validation/

This requires integration between your web framework and ``fanstatic``,
and making sure that the original resources (shipped in the ``resources``
directory in ``js.jquery_validate``) are published to some URL.